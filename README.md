# Temat
Wprowadzenie oraz dobór danych testowych

# Cel
Celem zajęć jest wyjaśnienie podstawowych pojęć związanych z procesem testowania oprogramowania: weryfikacja, walidacja, debugowanie, scenariusz testowy, plan testowania oraz uzupełnienie informacji nt. doboru danych testowych, wyboru zakresu i strategii testowania. W ramach zajęć zostaną wykonane ćwiczenia związane z tworzeniem scenariuszy testowych.

# Środowisko
Ćwiczenia będą wykonywane z wykorzystaniem dowolnych programów biurowych, także
dostępnych on-line.

# Zadania

## Wprowadzenie
Każdy przypadek testowy powinien uwzględniać:
1. Identyfikator,
2. Nazwę,
3. Warunki wstępne, stan,
4. Listę kroków,
5. Warunki końcowe, oczekiwany wynik.
Przypadek testowy może zawierać także dodatkowe informacje: np. powiązania z innymi testami, odwołanie do specyfikacji itp. Przypadki testowe mogą zostać umieszczone w formie tabelarycznej wg wzoru:

|Scenariusz:        |TC1:|
|-------------------|----|
|Powiązania:        |    |
|Specyfikacja:      |    |
|Warunki wstępne:   |    |
|Warunki końcowe:   |    |
|Krok Opis               |
|1                  |    |
|2                  |    |
|3                  |    |
|Wynik testu:       |    |

## Zadanie 1
Należy przygotować zestaw scenariuszy testowych dla suszarki do włosów. Suszarka jest zasilana z sieci, ma włącznik oraz dwa stopnie nawiewu. Scenariusz testowy powinien uwzględniać warunki wstępne, oczekiwany wynik, przebieg (kroki) testu. Praca w grupach.

### Albo
Alternatywnie można przygotować zestaw scenariuszy testowych, których celem jest sprawdzenie dostępności treści na platformie moodle. Dla tematu wprowadzenie powinien być dostępny działający link do projektu na gitlabie.

## Zadanie 2
Należy przygotować zestaw scenariuszy testowych dla wybranego fragmentu oprogramowania i wybranego zakresu funkcjonalności. Scenariusz testowy powinien uwzględniać warunki wstępne, oczekiwany wynik, przebieg (kroki) testu.

### Specyfikacja
W ramach systemu należy przygotować funkcję z oknem dialogowym, która pozwoli utworzyć nowego użytkownika w systemie. Ograniczenia:
1. Nazwa użytkownika od 4 do 16 znaków, tylko małe i wielkie litery,
2. Hasło od 8 do 16 znaków, małe i wielkie litery, cyfry,
3. Dodatkowo można podać numer telefonu oraz adres e-mail, zawartość pól dowolna, maksymalnie 32 znaki.